# Satellite Manager

Satellite Manager is a *Windows .NET 8* software capable of communication with a TCP IP Ground Station or USB LoRa Receiver Dongle (default usage).

It is able to communicate with the DemoSat satellite and display received data in a configurable manner.

# Insatller

If you only wish to install the software find and installer in:

- **installer** directory
  - `SatelliteManagerSetup.exe`

# Dependencies

- Project uses Visual Studio 2022
- Project utilizes NSIS for installer creation