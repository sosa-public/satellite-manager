﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace SatelliteManager.Utils
{
    class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value == true)
            {
                if (((string)parameter)?.ToLower() == "invert" || ((string)parameter)?.ToLower() == "inverted")
                {
                    return Visibility.Collapsed;
                }

                return Visibility.Visible;
            }
            else
            {
                if (((string)parameter)?.ToLower() == "invert" || ((string)parameter)?.ToLower() == "inverted")
                {
                    return Visibility.Visible;
                }

                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
