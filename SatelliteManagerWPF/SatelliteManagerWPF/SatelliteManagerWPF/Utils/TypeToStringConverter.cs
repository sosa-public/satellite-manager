﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Data;

namespace SatelliteManager.Utils
{
    class TypeToStringConverter : IValueConverter
    {
        public static TypeToStringConverter Instance { get; private set; }
        static TypeToStringConverter()
        {
            Instance = new TypeToStringConverter();
        }
        private TypeToStringConverter() { }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Type type)
            {
                return type.Name;
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string str = value as string;
            return Type.GetType("System."+str, false, true);
        }
    }
}
