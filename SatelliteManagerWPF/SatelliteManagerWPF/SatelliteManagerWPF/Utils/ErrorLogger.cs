﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SatelliteManager.Utils
{
    public class ErrorLogger
    {
        public static void LogException(Exception e, bool fails = false)
        {
            string logPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "SOSA",
                "Satellite Manager",
                "Error.log");
            var date = DateTime.UtcNow.ToString("yyyy-MM-ddThh-mm-ss");

            using (StreamWriter writer = new StreamWriter(logPath, append: true, encoding: Encoding.UTF8))
            {
                writer.WriteLine((fails ? "FAIL" : "") + "===" + date);
                writer.WriteLine(e.ToString());
            }
        }
    }
}
