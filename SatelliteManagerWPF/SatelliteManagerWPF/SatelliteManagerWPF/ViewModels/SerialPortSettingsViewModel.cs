﻿using SatelliteManager.Models;
using SatelliteManager.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text;
using System.Windows;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.ViewModels
{
    public class SerialPortSettingsViewModel : ObservableObject, IConnectionViewModel
    {
        private SerialPortSettings serialSettings;
        private bool portsNeedRefresh;

        public SerialPortSettingsViewModel()
        {
            this.serialSettings = new SerialPortSettings();
            this.State = ReceiverState.Disconnected;
        }

        public SerialPortSettingsViewModel(SerialPortSettings serialModel)
        {
            this.serialSettings = serialModel;
            this.State = ReceiverState.Disconnected;
        }

        private void Listener_Disconnected(object sender, EventArgs e)
        {
            this.State = ReceiverState.Disconnected;
            this.OnPropertyChanged("State");
        }

        public RelayCommand ConnectCommand { get; set; }
        public RelayCommand DisconnectCommand { get; set; }

        public SerialPortSettings SerialSettings
        {
            get { return serialSettings; }
            set { SetProperty(ref this.serialSettings, value, "SerialSettings"); }
        }

        public ReceiverState State { get; set; }
        public bool PortsNeedRefresh
        {
            get
            {
                return this.portsNeedRefresh;
            }

            set
            {
                if (value == true)
                {
                    this.OnPropertyChanged("AvailablePorts");
                }

                this.portsNeedRefresh = value;
            }
        }

        public string[] AvailablePorts
        {
            get
            {
                if (!AppController.Instance.Listener.IsListening)
                {
                    this.SelectedPort = null;
                }

                var names = SerialPort.GetPortNames().Distinct().ToArray();
                return names;
            }
        }

        public string SelectedPort
        {
            get
            {
                return this.serialSettings.PortName;
            }
            set
            {
                if (this.serialSettings.PortName != value)
                {
                    this.serialSettings.PortName = value;
                    OnPropertyChanged("SelectedPort");
                }
            }
        }

        public List<int> BaudOptions => new List<int>() { 4800, 9600, 14400, 19200, 38400, 57600, 115200, 128000, 256000 };

        public int SelectedBaud
        {
            get
            {
                return this.serialSettings.BaudRate;
            }
            set
            {
                if (this.SerialSettings.BaudRate != value)
                {
                    this.SerialSettings.BaudRate = value;
                    OnPropertyChanged("SelectedBaud");
                }
            }
        }

        public List<int> DataBitOptions => new List<int> { 5, 6, 7, 8, 9 };

        public int SelectedDataBits
        {
            get
            {
                return this.SerialSettings.DataBits;
            }

            set
            {
                if (this.SerialSettings.DataBits != value)
                {
                    this.SerialSettings.DataBits = value;
                    OnPropertyChanged("SelectedDataBits");
                }
            }
        }

        public StopBits SelectedStopBits
        {
            get
            {
                return this.SerialSettings.StopBits;
            }

            set
            {
                if (this.SerialSettings.StopBits != value)
                {
                    this.SerialSettings.StopBits = value;
                    OnPropertyChanged("SelectedStopBits");
                }
            }
        }

        public Parity SelectedParity
        {
            get
            {
                return this.SerialSettings.Parity;
            }

            set
            {
                if (this.SerialSettings.Parity != value)
                {
                    this.SerialSettings.Parity = value;
                    OnPropertyChanged("SelectedParity");
                }
            }
        }

        public Handshake SelectedFlowControl
        {
            get
            {
                return this.SerialSettings.FlowControl;
            }

            set
            {
                if (this.SerialSettings.FlowControl != value)
                {
                    this.SerialSettings.FlowControl = value;
                    OnPropertyChanged("SelectedFlowControl");
                }
            }
        }

        public void Connect()
        {
            if (AppController.Instance.SerialConnection != null && AppController.Instance.SerialConnection.IsOpen)
            {
                AppController.Instance.SerialConnection.Close();
            }

            SerialPort comPort;
            try
            {
                comPort = new SerialPort
                {
                    BaudRate = this.SelectedBaud,
                    Parity = this.SelectedParity,
                    DataBits = this.SelectedDataBits,
                    StopBits = this.SelectedStopBits,
                    Handshake = this.SelectedFlowControl,
                    PortName = this.SelectedPort,
                    DtrEnable = true,
                };

                comPort.Open();
            }
            catch (Exception ex)
            {
                if (ex is ArgumentException || ex is IOException || ex is InvalidOperationException ||
                    ex is UnauthorizedAccessException)
                {
                    MessageBox.Show("Could not connect!\nCheck your serial port settings and make sure nothing is connected to the COM port.", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                ErrorLogger.LogException(ex);
                throw;
            }

            AppController.Instance.SerialConnection = comPort;
            AppController.Instance.Listener.Disconnected += Listener_Disconnected;
            this.State = ReceiverState.Connected;
            this.OnPropertyChanged("State");
        }

        public void Disconnect()
        {
            if (AppController.Instance.SerialConnection != null && AppController.Instance.SerialConnection.IsOpen)
            {
                AppController.Instance.SerialConnection.Close();
            }

            AppController.Instance.Listener.Disconnected -= Listener_Disconnected;
            this.State = ReceiverState.Disconnected;
            this.OnPropertyChanged("State");
        }
    }
}
