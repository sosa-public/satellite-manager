﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.ViewModels
{
    class SettingsViewModel : ObservableObject
    {
        private AppController appc = AppController.Instance;
        private string csvLocationFolder;

        public SettingsViewModel()
        {
            this.CsvLocationFolder = appc.csvLogFolder;
            this.PickCsvLogFolderCommand = new RelayCommand(this.PickCsvLogFoder);

        }


        private void PickCsvLogFoder()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "CVS Log Folder Location";
            fbd.SelectedPath = this.CsvLocationFolder;
            fbd.ShowDialog();
            this.CsvLocationFolder = fbd.SelectedPath;
        }

        public RelayCommand PickCsvLogFolderCommand { get; private set; }

        public string CsvLocationFolder
        {
            get
            {
                return this.csvLocationFolder;
            }
            set
            {
                this.SetProperty(ref this.csvLocationFolder, value, "CsvLocationFolder");
                appc.csvLogFolder = value;
            }
        }
    }
}
