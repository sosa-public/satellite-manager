﻿using SatelliteManager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.ViewModels
{
    public class TerminalViewModel : ObservableObject
    {
        private string textLog;
        private string msg;
        private string lastMsg;
        private int textLogLines;

        public TerminalViewModel()
        {
            this.SendMessageCommand = new RelayCommand(this.SendMessage);
            this.RetrieveLastCommand = new RelayCommand(() =>
            {
                this.Message = this.lastMsg;
            });

            AppController.Instance.Listener.MsgLineReceived += Listener_MsgLineReceived;
            this.textLogLines = 0;
        }

        public string TextLog
        {
            get => textLog;
            set => SetProperty(ref this.textLog, value, "TextLog");
        }
        public string Message
        {
            get => msg;
            set => SetProperty(ref this.msg, value, "Message");
        }
        public string LineEnding { get; set; }
        public RelayCommand SendMessageCommand { get; set; }
        public RelayCommand RetrieveLastCommand { get; set; }

        private async void SendMessage()
        {
            var controller = AppController.Instance;
            this.lastMsg = this.Message;
            var newline = "\n"; //let's not have it blank anyway
            switch (LineEnding)
            {
                case @"\n":
                    newline = "\n";
                    break;
                case @"\r":
                    newline = "\r";
                    break;
                case @"\r\n":
                    newline = "\r\n";
                    break;
                default:
                    break;
            }

            if (controller.ConnectionStream != null)
            {
                byte[] buf = Encoding.ASCII.GetBytes(this.Message + newline);
                await controller.ConnectionStream.WriteAsync(buf, 0, buf.Length);
                await controller.ConnectionStream.FlushAsync();
                this.Message = string.Empty;
            }
        }

        private void Listener_MsgLineReceived(object sender, EventArgs e)
        {
            var payload = (e as ListenerMsgRecEventArgs).Data;
            if (!string.IsNullOrEmpty(payload))
            {
                var text = Encoding.ASCII.GetString(
                    Encoding.Convert(
                        Encoding.UTF8,
                        Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(string.Empty), new DecoderExceptionFallback()),
                        Encoding.UTF8.GetBytes(payload)
                        )
                    );
                this.TextLog += text + Environment.NewLine;
                this.textLogLines++;
                if (this.textLogLines > 250)
                {
                    var idx = this.textLog.IndexOf('\n');
                    this.TextLog = this.TextLog.Remove(0, idx + 1);
                    this.textLogLines--;
                }
            }
        }
    }
}
