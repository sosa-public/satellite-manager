﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SatelliteManager.ViewModels
{
    public enum ReceiverState
    {
        Active = 0,
        Connected,
        Disconnected
    }
    public interface IConnectionViewModel
    {
        public void Connect();
        public void Disconnect();
        public ReceiverState State { get; set; }
    }
}
