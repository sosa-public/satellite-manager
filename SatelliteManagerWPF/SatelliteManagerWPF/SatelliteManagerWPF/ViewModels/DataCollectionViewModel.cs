﻿using ControlzEx.Standard;
using SatelliteManager.Models;
using SatelliteManager.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Controls;
using System.Security.Principal;
using System.IO;
using System.Xml.Serialization;
using System.Windows.Media.TextFormatting;
using System.Windows.Threading;
using System.Windows;
using System.Threading.Tasks;
using NCalc;
using Expression = NCalc.Expression;
using System.Windows.Forms;
using System.Xml;
using Application = System.Windows.Application;
using System.Globalization;
using System.Windows.Input;
using System.Security.Cryptography;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.ViewModels
{
    public class DataCollectionViewModel : ObservableObject
    {
        private List<DataModel> dataCollection;
        private ObservableCollection<DataModel> editDataCollection;
        private bool isEditable;
        private int expectedDataLen;

        private List<List<DataModel>> dataCollections;

        public DataCollectionViewModel()
        {
            this.CsvDelimiter = ',';
            this.dataCollection = new List<DataModel>();

            AppController.Instance.Listener.MsgLineReceived += Listener_MsgLineReceived;
            this.IsEditable = false;
            this.HasError = false;

            this.plotControlViewModels = new ObservableCollection<PlotControlViewModel>();
            var defPlotVM = new PlotControlViewModel(this.dataCollection, "Default plot");
            this.plotControlViewModels.Add(defPlotVM);

            this.LoadDataModel();

            this.AddPlotView = new RelayCommand(() =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    var defPlotVM = new PlotControlViewModel(this.DataCollection, "Default plot");
                    this.plotControlViewModels.Add(defPlotVM);
                    this.SelectedPlotVM = defPlotVM;
                    this.OnPropertyChanged("PlotControlViewModels");
                    this.OnPropertyChanged("SelectedPlotVM");
                });
            });

            this.RemovePlotView = new RelayCommand<PlotControlViewModel>((p) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    this.plotControlViewModels.Remove(p);
                    this.OnPropertyChanged("PlotControlViewModels");
                });
            });

            this.ApplyModelCommand = new RelayCommand(this.ApplyModelChanges);
            this.AddElementCommand = new RelayCommand(this.AddElement);
            this.RemoveSelectedCommand = new RelayCommand<ObservableCollection<object>>(this.RemoveSelected);
            this.MoveSelectedDownCommand = new RelayCommand<DataModel>(this.MoveSelectedDown);
            this.MoveSelectedUpCommand = new RelayCommand<DataModel>(this.MoveSelectedUp);
            this.LoadModelCommand = new RelayCommand(() =>
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Title = "Load Saved Satellite Configuration";
                ofd.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";
                ofd.ShowDialog();
                if (!string.IsNullOrEmpty(ofd.FileName))
                {
                    this.LoadDataModel(ofd.FileName);
                }
            });
            this.SaveModelCommand = new RelayCommand(() =>
            {
                SaveFileDialog ofd = new SaveFileDialog();
                ofd.Title = "Save Satellite Configuration";
                ofd.Filter = "Text files (*.xml)|*.xml|All files (*.*)|*.*";
                ofd.ShowDialog();
                if (!string.IsNullOrEmpty(ofd.FileName))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<DataModel>), new XmlRootAttribute("DataModel"));
                    StreamWriter writer = new StreamWriter(ofd.FileName, false, UTF8Encoding.UTF8);
                    serializer.Serialize(writer, this.EditDataCollection.ToList());
                }
            });
            this.RestoreDefModelCommand = new RelayCommand(() =>
            {
                String defXmlDir = System.AppDomain.CurrentDomain.BaseDirectory;
                String defXmlPath = Path.Combine(defXmlDir, "DefaultSatellite.xml");
                if (!string.IsNullOrEmpty(defXmlPath))
                {
                    this.LoadDataModel(defXmlPath);
                }
            });
        }

        public RelayCommand AddPlotView { get; private set; }

        public RelayCommand<PlotControlViewModel> RemovePlotView { get; private set; }
        public RelayCommand ApplyModelCommand { get; private set; }
        public RelayCommand AddElementCommand { get; private set; }
        public RelayCommand<DataModel> MoveSelectedUpCommand { get; private set; }
        public RelayCommand<DataModel> MoveSelectedDownCommand { get; private set; }
        public RelayCommand<ObservableCollection<object>> RemoveSelectedCommand { get; private set; }
        public RelayCommand LoadModelCommand { get; private set; }
        public RelayCommand SaveModelCommand { get; private set; }
        public RelayCommand RestoreDefModelCommand { get; private set; }
        public PlotControlViewModel SelectedPlotVM { get; set; }

        public TerminalViewModel TerminalVM { get; set; }
        public static ObservableCollection<string> AvailableDataTypes { get; } = new ObservableCollection<string>()
        {
            "SByte",    //typeof(SByte),
            "Byte",     //typeof(Byte),
            "Int16",    //typeof(Int16),
            "UInt16",   //typeof(UInt16),
            "Int32",    //typeof(Int32),
            "UInt32",   //typeof(UInt32),
            "Int64",    //typeof(Int64),
            "UInt64",   //typeof(UInt64),
            "Single",   //typeof(Single),
            "Double",   //typeof(Double),
            "Decimal",  //typeof(Decimal)
        };

        public bool IsEditable { get => isEditable; set => SetProperty(ref this.isEditable, value, "IsEditable"); }
        public bool HasError { get; set; }
        public static string ErrorText => "The defined packet model has errors! Chack if every field has a unique Name and a Type defined!";
        public bool IsCSV { get; set; }
        public char CsvDelimiter { get; set; }
        /// <summary>
        /// This is one packet which can be definned in custom manner
        /// </summary>
        public List<DataModel> DataCollection
        {
            get => dataCollection;
            private set => SetProperty(ref this.dataCollection, value, "DataCollection");
        }
        public ObservableCollection<DataModel> EditDataCollection
        {
            get => editDataCollection;
            private set => SetProperty(ref this.editDataCollection, value, "EditDataCollection");
        }

        private ObservableCollection<PlotControlViewModel> plotControlViewModels;

        public ObservableCollection<PlotControlViewModel> PlotControlViewModels
        {
            get { return this.plotControlViewModels; }
            set { SetProperty(ref this.plotControlViewModels, value, "PlotControlViewModels"); }
        }


        internal void LoadDataModel(string savedPathToModel = null)
        {
            if (this.DataCollection == null)
            {
                this.DataCollection = new List<DataModel>();
            }

            this.DataCollection.Clear();

            // Deserialize saved data model
            string pathToModel = string.Empty;
            if (string.IsNullOrEmpty(savedPathToModel))
            {
                pathToModel = AppController.Instance.LastDataModelPath;
            }
            else
            {
                pathToModel = savedPathToModel;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(List<DataModel>), new XmlRootAttribute("DataModel"));
            XmlReader lastFile;

            if (File.Exists(pathToModel))
            {
                lastFile = XmlReader.Create(pathToModel);
            }
            else
            {
                lastFile = XmlReader.Create("DefaultSatellite.xml");
            }

            if (serializer.CanDeserialize(lastFile))
            {
                var deserialized = (List<DataModel>)serializer.Deserialize(lastFile);
                this.EditDataCollection = new ObservableCollection<DataModel>(deserialized);
                foreach (var item in this.EditDataCollection)
                {
                    if (item.DataType == null)
                    {
                        item.DataTypeString = "Byte";
                    }

                    if (item.DataType.IsValueType)
                    {
                        this.expectedDataLen += Marshal.SizeOf(item.DataType);
                    }
                }
            }
            else
            {
                return;
            }
            lastFile.Close();

            OnPropertyChanged("EditDataCollection");
            this.ApplyModelChanges();
        }

        private void ApplyTransformOnItem(DataModel item)
        {
            // transform if defined
            if (!string.IsNullOrEmpty(item.Transform))
            {
                Expression exp = new Expression(item.Transform);
                exp.Parameters["X"] = item.Data;
                exp.Parameters["x"] = item.Data;
                try
                {
                    var newResult = exp.Evaluate();
                    item.Data = (double)Convert.ChangeType(newResult, typeof(double));
                }
                catch (Exception ex)
                {
                    if (ex is OverflowException || ex is InvalidCastException || ex is ArgumentNullException || ex is EvaluationException || ex is ArgumentException)
                    {
                        // do something maybe
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        private void Listener_MsgLineReceived(object sender, EventArgs e)
        {
            if (this.IsEditable)
            {
                return;
            }

            var payload = (e as ListenerMsgRecEventArgs).Data;
            if (payload != null)
            {
                byte[] decoded;
                try
                {
                    if (!string.IsNullOrEmpty(payload))
                    {
                        if (this.IsCSV)
                        {
                            if (!this.IsEditable)
                            {
                                char delimiter = this.CsvDelimiter;
                                var styles = NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowDecimalPoint;
                                var provider = NumberFormatInfo.InvariantInfo;

                                string[] fields = payload.Split(delimiter);
                                int len = fields.Length < this.DataCollection.Count ? fields.Length : this.DataCollection.Count;
                                for (int i = 0; i < len; i++)
                                {
                                    this.DataCollection[i].Data = double.Parse(fields[i], styles, provider);
                                    this.ApplyTransformOnItem(this.DataCollection[i]);
                                    this.EditDataCollection[i].Data = this.DataCollection[i].Data; // only so the table displays data
                                }
                            }
                        }
                        else
                        {
                            decoded = Convert.FromBase64String(payload);
                            // only decoded packects of the expected length are accepted, ISM bands are weird
                            if (decoded.Length == this.expectedDataLen)
                            {
                                SatelliteModel.ParseBytes(decoded.AsSpan<byte>(), ref this.dataCollection);
                                double unixTimestamp = (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                                AppController.Instance.csvlogger.LogDataCollection(ref this.dataCollection, unixTimestamp, payload);
                                if (!this.IsEditable)
                                {
                                    for (int i = 0; i < this.dataCollection.Count; i++)
                                    {
                                        try
                                        {
                                            this.ApplyTransformOnItem(this.DataCollection[i]);
                                            this.EditDataCollection[i].Data = this.DataCollection[i].Data; // only so the table displays numbers
                                        }
                                        catch (Exception)
                                        {
                                            throw;
                                        }
                                    }
                                }

                            }
                        }

                        OnPropertyChanged("DataCollection");
                        this.UpdatePlotData(this.dataCollection);
                    }
                }
                catch (FormatException)
                {
                    // not base64 but whatever, do nothing it is caught in terminal too to see quickly
                }
            }
        }

        private bool ValidateModel()
        {
            if (this.EditDataCollection.Select(p => p.Name).Distinct().Count() != this.EditDataCollection.Count())
            {
                return false;
            }

            foreach (var item in this.EditDataCollection)
            {
                if (string.IsNullOrEmpty(item.Name) ||
                    string.IsNullOrWhiteSpace(item.Name) ||
                    item.DataType == null)
                {
                    return false;
                }
            }

            return true;
        }

        internal void ApplyModelChanges()
        {
            if (this.ValidateModel())
            {
                this.DataCollection = this.EditDataCollection.Select(x => x.Copy()).ToList();
                this.expectedDataLen = 0;
                foreach (var item in this.DataCollection)
                {
                    if (item.DataType.IsValueType)
                    {
                        this.expectedDataLen += Marshal.SizeOf(item.DataType);
                    }
                }

                // plot models reinit
                var names = this.plotControlViewModels.Select(x => x.Title).ToList();
                List<Dictionary<string, bool>> areVisible = this.plotControlViewModels.Select(p => p.AvailableData.ToDictionary(x => x.Name, x => x.IncludeInPlot)).ToList();

                this.plotControlViewModels.Clear();
                for (int i = 0; i < names.Count; i++)
                {
                    var toadd = new PlotControlViewModel(this.DataCollection, names[i]);
                    foreach (var item in toadd.AvailableData)
                    {
                        bool prevVisibility = false;
                        if (areVisible[i].TryGetValue(item.Name, out prevVisibility))
                        {
                            if (prevVisibility)
                            {
                                item.IncludeInPlot = true;
                                var ax = toadd.PlotModel.Axes.Where(a => a.Key == item.Unit).FirstOrDefault();
                                if (ax != null)
                                {
                                    ax.IsAxisVisible = true;
                                }
                            }
                        }
                    }

                    this.plotControlViewModels.Add(toadd);
                }

                AppController.Instance.csvlogger = new CsvLogger(this.DataCollection);

                this.OnPropertyChanged("PlotControlViewModels");
                this.OnPropertyChanged("DataCollection");
                this.HasError = false;
                this.OnPropertyChanged("HasError");
                this.IsEditable = false;
            }
            else
            {
                this.HasError = true;
                this.OnPropertyChanged("HasError");
            }
        }

        private void AddElement()
        {
            this.EditDataCollection.Add(new DataModel()
            {
                Name = "p" + this.editDataCollection.Count,
                DataTypeString = "Byte"
            });
            this.OnPropertyChanged("EditDataCollection");
        }

        private void RemoveSelected(ObservableCollection<object> selected)
        {
            var toremove = selected.ToList();
            foreach (DataModel item in toremove)
            {
                this.EditDataCollection.Remove(item);
            }

            this.OnPropertyChanged("EditDataCollection");
        }

        public void UpdatePlotData(IList<DataModel> newData)
        {
            Application.Current?.Dispatcher.Invoke(() =>
            {
                for (int i = 0; i < newData.Count; i++)
                {
                    foreach (PlotControlViewModel item in this.plotControlViewModels)
                    {
                        item.AddDataPoint(newData[i]);
                    }
                }

            });
        }

        private void MoveSelectedDown(DataModel selected)
        {
            var idx = this.editDataCollection.IndexOf(selected);
            if (idx + 1 != this.editDataCollection.Count)
            {
                this.EditDataCollection.Move(idx, idx + 1);
                this.OnPropertyChanged("EditDataCollection");
            }
        }
        private void MoveSelectedUp(DataModel selected)
        {
            var idx = this.editDataCollection.IndexOf(selected);
            if (idx - 1 >= 0)
            {
                this.EditDataCollection.Move(idx, idx - 1);
                this.OnPropertyChanged("EditDataCollection");
            }
        }
    }
}
