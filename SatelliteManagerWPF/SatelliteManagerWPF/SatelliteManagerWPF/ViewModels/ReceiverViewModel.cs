﻿using SatelliteManager.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.ViewModels
{
    public class ReceiverViewModel : ObservableObject
    {
        public ReceiverViewModel()
        {
            this.RestoreSerialVM();
            this.RestoreTcpVM();

            this.ConnectCommand = new RelayCommand(this.Connect);
            this.DisconnectCommand = new RelayCommand(this.Disconnect);

            AppController.Instance.Listener.MsgLineReceived += async (o, e) =>
            {
                if (this.istcp)
                {
                    this.TcpSettingsViewModel.State = ReceiverState.Active;
                    this.OnPropertyChanged("State");
                    await Task.Delay(200);
                    this.TcpSettingsViewModel.State = ReceiverState.Connected;
                    this.OnPropertyChanged("State");
                }
                else
                {
                    this.SerialPortSettings.State = ReceiverState.Active;
                    this.OnPropertyChanged("State");
                    await Task.Delay(200);
                    this.SerialPortSettings.State = ReceiverState.Connected;
                    this.OnPropertyChanged("State");
                }
            };
        }

        private bool istcp;
        public RelayCommand ConnectCommand { get; private set; }
        public RelayCommand DisconnectCommand { get; private set; }

        public bool IsTCP
        {
            get
            {
                return istcp;
            }
            set
            {
                if (value == true)
                {

                }

                SetProperty(ref this.istcp, value, "IsTCP");
            }
        }
        public ReceiverState State
        {
            get
            {
                if (this.IsTCP)
                {
                    return this.TcpSettingsViewModel.State;
                }
                else
                {
                    return this.SerialPortSettings.State;
                }
            }
        }
        private void Connect()
        {
            if (this.istcp)
            {
                this.TcpSettingsViewModel.Connect();
            }
            else
            {
                this.SerialPortSettings.Connect();
            }

            this.OnPropertyChanged("State");
        }
        private void Disconnect()
        {
            if (this.istcp)
            {
                this.TcpSettingsViewModel.Disconnect();
            }
            else
            {
                this.SerialPortSettings.Disconnect();
            }

            this.OnPropertyChanged("State");
        }
        public SerialPortSettingsViewModel SerialPortSettings { get; private set; }
        public TcpSettingsViewModel TcpSettingsViewModel { get; private set; }

        private void RestoreSerialVM()
        {
            var appstate = AppController.Instance.appState;
            var serialModel = new SerialPortSettings
            {
                BaudRate = appstate.SerialBaud,
                DataBits = appstate.SerialDataBits
            };
            if (Enum.TryParse<Parity>(appstate.SerialParity, true, out Parity lastParity))
            {
                serialModel.Parity = lastParity;
            }
            else
            {
                serialModel.Parity = Parity.None;
            }

            if (Enum.TryParse<StopBits>(appstate.SerialStopBits, true, out StopBits lastStop))
            {
                serialModel.StopBits = lastStop;
            }
            else
            {
                serialModel.StopBits = StopBits.None;
            }

            if (Enum.TryParse<Handshake>(appstate.SerialFlow, true, out Handshake lastFlow))
            {
                serialModel.FlowControl = lastFlow;
            }
            else
            {
                serialModel.FlowControl = Handshake.None;
            }

            this.SerialPortSettings = new SerialPortSettingsViewModel(serialModel);
        }

        private void RestoreTcpVM()
        {
            var appstate = AppController.Instance.appState;
            this.IsTCP = appstate.IsTcp;
            this.TcpSettingsViewModel = new TcpSettingsViewModel
            {
                IpString = appstate.IpString,
                Port = appstate.IpPort
            };
        }
    }
}
