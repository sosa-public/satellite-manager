﻿using MvvmCross.Core;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using SatelliteManager.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Windows.Controls.Primitives;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.ViewModels
{
    public class PlotControlViewModel : ObservableObject
    {
        private ObservableCollection<DataPoint> dataPoints;
        private PlotModel plotModel;
        private uint maximumPoints;

        public delegate void AddDataPointDelegate(DataModel newData);

        public PlotControlViewModel(List<DataModel> initialDataModels, string title = "")
        {
            this.Title = title;
            this.plotModel = new PlotModel();
            this.AvailableData = new ObservableCollection<DataModel>(initialDataModels.Select(x => x.Copy()));
            this.CheckedChanged = new RelayCommand<object>(this.ChangeVisibilities);
            this.maximumPoints = 1000;

            // create axes according to units present in datamodel
            this.plotModel.Axes.Add(new DateTimeAxis() { Key = "timeAxis", Position = AxisPosition.Bottom, MajorGridlineStyle = LineStyle.Dot });
            int tier = 0;
            foreach (DataModel item in this.AvailableData)
            {
                if (this.plotModel.Axes.Where(a => a.Key == item.Unit).Any())
                {
                    continue;
                }

                var ax = new LinearAxis
                {
                    Key = item.Unit,
                    Position = AxisPosition.Left,
                    Unit = item.Unit,
                    PositionTier = tier++,
                    IsAxisVisible = false,
                    MajorGridlineStyle = LineStyle.Dot,
                };
                this.plotModel.Axes.Add(ax);
            }
        }

        public RelayCommand<object> CheckedChanged { get; private set; }

        public uint MaximumPoints
        {
            get
            {
                return maximumPoints;
            }
            set
            {
                if (value > 0)
                {
                    if (value > this.maximumPoints)
                    {
                        SetProperty(ref this.maximumPoints, value, "MaximumPoints");
                        this.RefreshVisibilities();
                    }
                    else
                    {
                        SetProperty(ref this.maximumPoints, value, "MaximumPoints");
                    }
                }
            }
        }

        public ObservableCollection<DataPoint> DataPoints
        {
            get { return dataPoints; }
            internal set { SetProperty(ref this.dataPoints, value, "DataPoints"); }
        }

        public String Title { get; set; }
        public ObservableCollection<DataModel> AvailableData { get; set; }

        public PlotModel PlotModel
        {
            get { return plotModel; }
            set { SetProperty(ref this.plotModel, value, "PlotModel"); }
        }
        public void AddDataPoint(DataModel newData)
        {
            LineSeries existingLineSeries = null;
            for (int i = 0; i < this.plotModel.Series.Count; i++)
            {
                if (this.plotModel.Series[i].Title == newData.Name)
                {
                    existingLineSeries = this.plotModel.Series[i] as LineSeries;
                    break;
                }
            }

            if (existingLineSeries == null)
            {
                existingLineSeries = new LineSeries
                {
                    XAxisKey = "timeAxis",
                    YAxisKey = newData.Unit,
                    Title = newData.Name,
                    LineStyle = LineStyle.Solid,
                    StrokeThickness = 2,
                    MarkerType = MarkerType.Plus,
                    MarkerSize = 5,
                    MarkerStroke = OxyColor.Parse("#000000"),
                    IsVisible = this.AvailableData.SingleOrDefault(x => x.Name == newData.Name).IncludeInPlot,
                    CanTrackerInterpolatePoints = false,
                };
                this.plotModel.Series.Add(existingLineSeries);
            }

            var x = DateTimeAxis.ToDouble(DateTime.Now);
            var y = Convert.ToDouble(newData.Data);
            while (existingLineSeries.Points.Count >= this.MaximumPoints)
            {
                existingLineSeries.Points.RemoveAt(0);
            }

            existingLineSeries.Points.Add(new DataPoint(x, y));

            this.plotModel.InvalidatePlot(true);
        }

        internal void RefreshVisibilities()
        {
            foreach (var series in this.plotModel.Series)
            {
                bool? wantedVisibility = this.AvailableData.SingleOrDefault(x => x.Name == series.Title)?.IncludeInPlot;
                series.IsVisible = wantedVisibility ?? false;

                string desiredUnit = this.AvailableData.Where(d => d.Name == series.Title).FirstOrDefault()?.Unit;
                if (desiredUnit != null)
                {
                    var axis = this.plotModel.Axes.Where(a => a.Unit == desiredUnit).FirstOrDefault();
                    if (axis != null)
                    {
                        var showaxis = (this.AvailableData.Where(d => d.Unit == axis.Unit && d.IncludeInPlot).Any());
                        axis.IsAxisVisible = showaxis;
                    }
                }
            }

            this.plotModel.InvalidatePlot(true);
        }
        private void ChangeVisibilities(object obj)
        {
            var str = obj as string;
            if (!string.IsNullOrEmpty(str))
            {
                if (this.plotModel.Series.Where(s => s.Title == str).FirstOrDefault() is LineSeries found)
                {
                    bool? wantedVisibility = this.AvailableData.SingleOrDefault(x => x.Name == str)?.IncludeInPlot;
                    found.IsVisible = wantedVisibility ?? false;
                }

                string desiredUnit = this.AvailableData.Where(d => d.Name == str).FirstOrDefault()?.Unit;

                var axis = this.plotModel.Axes.Where(a => a.Unit == desiredUnit && !a.IsHorizontal()).FirstOrDefault();
                if (axis != null)
                {
                    var showaxis = (this.AvailableData.Where(d => d.Unit == axis.Unit && d.IncludeInPlot).Any());
                    axis.IsAxisVisible = showaxis;
                }

                this.plotModel.InvalidatePlot(true);
            }
        }
    }
}
