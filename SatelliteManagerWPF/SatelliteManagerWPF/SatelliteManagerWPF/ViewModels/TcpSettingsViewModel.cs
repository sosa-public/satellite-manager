﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.ViewModels
{
    public class TcpSettingsViewModel : ObservableObject, IConnectionViewModel
    {
        public TcpSettingsViewModel()
        {
            this.State = ReceiverState.Disconnected;
        }

        private string ipString;

        public string IpString
        {
            get { return ipString; }
            set { SetProperty(ref this.ipString, value, "IpString"); }
        }

        private int port;

        public int Port
        {
            get { return port; }
            set { SetProperty(ref this.port, value, "Port"); }
        }

        public ReceiverState State { get; set; }

        public void Connect()
        {
            var controller = AppController.Instance;

            var endpoint = new IPEndPoint(IPAddress.Parse(this.IpString), this.Port);
            var client = new System.Net.Sockets.TcpClient();
            try
            {
                client.Connect(endpoint);
                controller.TcpConnection = client;
            }
            catch (System.Net.Sockets.SocketException)
            {
                // Do nothing I guess... experimental feature
            }

            this.State = ReceiverState.Connected;
            this.OnPropertyChanged("State");
        }

        public void Disconnect()
        {
            AppController.Instance.TcpConnection.Dispose();

            this.State = ReceiverState.Disconnected;
            this.OnPropertyChanged("State");
        }
    }
}
