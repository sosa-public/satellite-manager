﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SatelliteManager.Views
{
    /// <summary>
    /// Interaction logic for PlotControl.xaml
    /// </summary>
    public partial class PlotControl : UserControl
    {
        //private Cursor _cursor;
        public PlotControl()
        {
            InitializeComponent();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            this.thisPlot.Model = null;
        }

        /*
        private void OnResizeThumbDragStarted(object sender, DragStartedEventArgs e)
        {
            _cursor = Cursor;
            Cursor = Cursors.SizeNWSE;
        }

        private void OnResizeThumbDragCompleted(object sender, DragCompletedEventArgs e)
        {
            Cursor = _cursor;
        }

        private void OnResizeThumbDragDelta(object sender, DragDeltaEventArgs e)
        {
            double yAdjust = sizableContent.Height + e.VerticalChange;
            double xAdjust = sizableContent.Width + e.HorizontalChange;

            //make sure not to resize to negative width or heigth            
            xAdjust = (sizableContent.ActualWidth + xAdjust) > sizableContent.MinWidth ? xAdjust : sizableContent.MinWidth;
            yAdjust = (sizableContent.ActualHeight + yAdjust) > sizableContent.MinHeight ? yAdjust : sizableContent.MinHeight;

            //sizableContent.Width = xAdjust;
            sizableContent.Height = yAdjust;
        }
        */
    }
}
