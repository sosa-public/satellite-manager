﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NCalc;
using SatelliteManager.Utils;

namespace SatelliteManager.Models
{
    public class SatelliteModel
    {
        public static void ParseBytes(ReadOnlySpan<byte> bytes, ref List<DataModel> currentModel)
        {
            int offset = 0;
            foreach (DataModel item in currentModel)
            {
                var parsed = Activator.CreateInstance(item.DataType);
                try
                {
                    switch (item)
                    {
                        case DataModel m when m.DataType == typeof(Byte):
                            parsed = (Byte)bytes[offset];
                            offset++;
                            break;
                        case DataModel m when m.DataType == typeof(SByte):
                            parsed = (SByte)bytes[offset];
                            offset++;
                            break;
                        case DataModel m when m.DataType == typeof(Int16):
                            parsed = BitConverter.ToInt16(bytes.Slice(offset));
                            offset += 2;
                            break;
                        case DataModel m when m.DataType == typeof(UInt16):
                            parsed = BitConverter.ToUInt16(bytes.Slice(offset));
                            offset += 2;
                            break;
                        case DataModel m when m.DataType == typeof(Int32):
                            parsed = BitConverter.ToInt32(bytes.Slice(offset));
                            offset += 4;
                            break;
                        case DataModel m when m.DataType == typeof(UInt32):
                            parsed = BitConverter.ToUInt32(bytes.Slice(offset));
                            offset += 4;
                            break;
                        case DataModel m when m.DataType == typeof(Int64):
                            parsed = BitConverter.ToInt64(bytes.Slice(offset));
                            offset += 8;
                            break;
                        case DataModel m when m.DataType == typeof(UInt64):
                            parsed = BitConverter.ToUInt64(bytes.Slice(offset));
                            offset += 8;
                            break;
                        case DataModel m when m.DataType == typeof(Single):
                            parsed = BitConverter.ToSingle(bytes.Slice(offset));
                            offset += 4;
                            break;
                        case DataModel m when m.DataType == typeof(Double):
                            parsed = BitConverter.ToDouble(bytes.Slice(offset));
                            offset += 8;
                            break;
                        case DataModel m when m.DataType == typeof(Decimal):
                            parsed = BitConverter.ToInt64(bytes.Slice(offset));
                            offset += 16;
                            break;
                        default:
                            throw new Exception("Error during packet conversion");
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    // ignore, user error proceed according to settings
                }
                catch (Exception e)
                {
                    ErrorLogger.LogException(e, fails:true);
                    throw;
                }
                
                item.Data = (double)Convert.ChangeType(parsed, typeof(double));
            }
        }
    }
}
