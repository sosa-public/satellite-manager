﻿using SatelliteManager.Utils;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Xml.Serialization;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.ComponentModel;

namespace SatelliteManager.Models
{
    public class DataModel : ObservableObject
    {
        private string name;

        [XmlAttribute]
        public string Name
        {
            get { return name; }
            set { SetProperty(ref this.name, value, "Name"); }
        }

        private string unit;

        [XmlAttribute]
        public string Unit
        {
            get { return unit; }
            set
            {
                string newVal = string.IsNullOrEmpty(value) ? null : value;
                SetProperty(ref this.unit, newVal, "Unit");
            }
        }

        private Type dataType;

        [XmlIgnore]
        [NotMapped]
        public Type DataType
        {
            get { return dataType; }
            set { SetProperty(ref this.dataType, value, "DataType"); }
        }

        [XmlAttribute]
        [NotMapped]
        public string DataTypeString
        {
            get
            {
                return (string)TypeToStringConverter.Instance.Convert(this.dataType, typeof(string), null, CultureInfo.InvariantCulture);
            }
            set
            {
                Type desiredType = (Type)TypeToStringConverter.Instance.ConvertBack(value, typeof(Type), null, CultureInfo.InvariantCulture);
                this.SetProperty(ref this.dataType, desiredType, "DataType");
                this.OnPropertyChanged("DataTypeString");
            }
        }

        private bool incInPlot;

        [XmlIgnore]
        [NotMapped]
        public bool IncludeInPlot
        {
            get { return incInPlot; }
            set { SetProperty(ref this.incInPlot, value, "IncludeInPlot"); }
        }

        private double data;

        [XmlIgnore]
        public double Data
        {
            get { return data; }
            set { SetProperty(ref this.data, value, "Data"); }
        }

        private string transform;

        [XmlAttribute]
        [NotMapped]
        public string Transform
        {
            get
            {
                return this.transform;
            }
            set
            {
                SetProperty(ref this.transform, value, "Transform");
            }
        }

        public DataModel Copy()
        {
            return new DataModel()
            {
                Name = this.name,
                Unit = this.unit,
                DataType = this.dataType,
                Data = this.data,
                IncludeInPlot = this.incInPlot,
                Transform = this.transform,
            };
        }
    }
}
