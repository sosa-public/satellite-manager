﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SatelliteManager.Models
{
    public class CsvLogger : IDisposable
    {
        private StreamWriter csvWriter;
        private char delimiter;
        public CsvLogger(IList<DataModel> dataModels, char delim = ';')
        {
            string filename = string.Empty;
            string dir = AppController.Instance.csvLogFolder;
            if (string.IsNullOrEmpty(dir))
            {
                dir = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    "SOSA",
                    "Satellite Manager",
                    "CsvLogs");
                AppController.Instance.csvLogFolder = dir;
            }

            filename = Path.Combine(
                dir,
                DateTime.UtcNow.ToString("yyyy-MM-ddTHH-mm-ss") + "-Log.csv");

            Directory.CreateDirectory(new FileInfo(filename).DirectoryName);
            this.delimiter = delim;
            this.csvWriter = new StreamWriter(filename, false, Encoding.UTF8);

            string headerline = string.Empty;
            headerline += "Timestamp_s" + this.delimiter;
            headerline += "Original" + this.delimiter;
            foreach (var item in dataModels)
            {
                headerline += item.Name;
                headerline += string.IsNullOrEmpty(item.Unit) ? string.Empty : ("_" + item.Unit);
                headerline += this.delimiter;
            }

            headerline.Trim(this.delimiter);
            this.csvWriter.WriteLine(headerline);
            this.csvWriter.Flush();
        }

        ~CsvLogger()
        {
            this.Dispose();
        }

        public void Dispose()
        {
            try
            {
                this.csvWriter.Close();
                this.csvWriter.Dispose();
            }
            catch (ObjectDisposedException)
            {
                // probably expected already disposed writer called by destructor
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void LogDataCollection(ref List<DataModel> newData, double unixtime, string original)
        {
            string line = string.Empty;
            line += unixtime.ToString(System.Globalization.CultureInfo.InvariantCulture) + this.delimiter;
            line += original + this.delimiter;
            foreach (var item in newData)
            {
                line += item.Data.ToString(System.Globalization.CultureInfo.InvariantCulture) + this.delimiter;
            }

            line.TrimEnd(this.delimiter);
            csvWriter.WriteLine(line);
            csvWriter.Flush();
        }
    }
}
