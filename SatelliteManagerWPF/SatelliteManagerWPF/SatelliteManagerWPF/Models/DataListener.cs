﻿using SatelliteManager.Utils;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SatelliteManager.Models
{
    class DataListener
    {
        private StreamReader reader;

        public DataListener()
        {
        }

        public event EventHandler MsgLineReceived;
        public event EventHandler Disconnected;

        public bool IsListening { get; set; }

        protected virtual void OnMsgLineReceived(ListenerMsgRecEventArgs e)
        {
            EventHandler h = this.MsgLineReceived;

            h?.Invoke(this, e);
        }

        protected virtual void OnDisconnected(EventArgs e)
        {
            EventHandler h = this.Disconnected;

            h?.Invoke(this, e);
        }

        public void RestartListening(Stream stream)
        {
            this.reader = new StreamReader(stream);
            Task.Factory.StartNew(this.Listening, TaskCreationOptions.LongRunning);
        }

        private async void Listening()
        {
            this.IsListening = true;
            while (true)
            {
                try
                {
                    if (this.reader != null)
                    {
                        String rec = String.Empty;
                        
                        try
                        {
                            rec = await this.reader.ReadLineAsync();
                        }
                        catch (Exception) // something happened, most probably the connection was closed as in TCP
                        {
                            this.OnDisconnected(new EventArgs());
                            this.IsListening = false;
                            return;
                        }

                        if (String.IsNullOrEmpty(rec) == false)
                        {
                            this.OnMsgLineReceived(new ListenerMsgRecEventArgs(rec));
                        }
                    }
                }
                catch (Exception e)
                {
                    if (e is OperationCanceledException ||
                        e is ObjectDisposedException ||
                        e is UnauthorizedAccessException)
                    {
                        this.OnDisconnected(new EventArgs());
                        this.IsListening = false;
                        return;
                    }
                    else if (e is IOException && e.Message.Contains("forcibly closed"))
                    {
                        this.OnDisconnected(new EventArgs());
                        this.IsListening = false;
                        return;
                    }
                    else
                    {
                        this.OnDisconnected(new EventArgs());
                        this.IsListening = false;
                        ErrorLogger.LogException(e);
                        throw;
                    }
                }
            }
        }
    }

    internal class ListenerMsgRecEventArgs : EventArgs
    {
        public ListenerMsgRecEventArgs(string data)
        {
            Data = data;
        }

        public string Data { get; set; }
    }
}
