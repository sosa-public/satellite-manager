﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace SatelliteManager.Models
{
    public class SerialPortSettings
    {
        public int BaudRate { get; set; }
        public string PortName { get; set; }
        public int DataBits { get; set; }
        public StopBits StopBits { get; set; }
        public Handshake FlowControl { get; set; }
        public Parity Parity { get; set; }
    }
}
