﻿using MvvmCross.Base;
using SatelliteManager.Models;
using SatelliteManager.Properties;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using SatelliteManager.Utils;

namespace SatelliteManager
{
    public class AppController
    {
        internal Settings appState;
        internal CsvLogger csvlogger;
        internal string csvLogFolder;

        private AppController()
        {
            this.Listener = new DataListener();
            Settings.Default.Upgrade();
            this.appState = Settings.Default;
            this.csvLogFolder = this.appState.CsvLocation;

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            ErrorLogger.LogException(ex, true);
        }

        public static AppController Instance { get { return Nested.instance; } }

        private class Nested
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
            static Nested()
            {
            }

            internal static readonly AppController instance = new AppController();
        }

        public ulong SessionId { get; internal set; }

        private SerialPort serialConnection;
        private TcpClient tcpConnection;

        internal string LastDataModelPath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "SOSA",
            "Satellite Manager",
            "LastDataModel.xml");

        internal string DbFilePath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
            "SOSA",
            "Satellite Manager",
            "DataArchive.db");

        internal DataListener Listener;
        internal Stream ConnectionStream;
        internal SerialPort SerialConnection
        {
            get
            {
                return this.serialConnection;
            }
            set
            {
                this.serialConnection?.Close();
                this.serialConnection?.Dispose();
                this.serialConnection = value;
                this.ConnectionStream = this.serialConnection.BaseStream;

                this.Listener.RestartListening(this.ConnectionStream);
            }
        }

        internal TcpClient TcpConnection
        {
            get
            {
                return this.tcpConnection;
            }
            set
            {
                this.tcpConnection?.Close();
                this.tcpConnection?.Dispose();
                this.tcpConnection = value;
                this.ConnectionStream = this.tcpConnection.GetStream();

                this.Listener.RestartListening(this.ConnectionStream);
            }
        }
    }
}
