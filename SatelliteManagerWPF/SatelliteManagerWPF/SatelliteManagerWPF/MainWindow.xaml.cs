﻿using SatelliteManager.ViewModels;
using SatelliteManager.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Xml.Serialization;
using SatelliteManager.Models;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using CommunityToolkit.Mvvm;
using CommunityToolkit.Mvvm.Input;

namespace SatelliteManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        private ReceiverViewModel receiverVM;
        private TerminalViewModel termVM;
        private DataCollectionViewModel dataColVM;

        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = this;

            // create view models
            this.receiverVM = new ReceiverViewModel();
            this.termVM = new TerminalViewModel();
            this.dataColVM = new DataCollectionViewModel() { TerminalVM = this.termVM };

            // set Home as starting
            this.mainContent.Content = new ReceiverView() { DataContext = this.receiverVM };
            this.homeActive.Visibility = Visibility.Visible;
            this.satActive.Visibility = Visibility.Hidden;
            this.dataActive.Visibility = Visibility.Hidden;
            this.settingsActive.Visibility = Visibility.Hidden;
            this.termActive.Visibility = Visibility.Hidden;

            this.CsvLocationFolder = AppController.Instance.csvLogFolder;
            //this.PickCsvLogFolderCommand = new Command(this.PickCsvLogFoder);
            this.PickCsvLogFolderCommand = new RelayCommand(this.PickCsvLogFoder);
        }
        //public Command PickCsvLogFolderCommand { get; private set; }
        public RelayCommand PickCsvLogFolderCommand { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;
        // This method is called by the Set accessor of each property.  
        // The CallerMemberName attribute that is applied to the optional propertyName  
        // parameter causes the property name of the caller to be substituted as an argument.  
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ReceiverViewModel ReceiverVM
        {
            get { return this.receiverVM; }
        }

        private void Home_Clicked(object sender, RoutedEventArgs e)
        {
            if (this.homeActive.IsVisible == false)
            {
                this.mainContent.Content = new ReceiverView() { DataContext = this.receiverVM };
                this.homeActive.Visibility = Visibility.Visible;
                this.satActive.Visibility = Visibility.Hidden;
                this.dataActive.Visibility = Visibility.Hidden;
                this.settingsActive.Visibility = Visibility.Hidden;
                this.termActive.Visibility = Visibility.Hidden;
            }
        }

        private void Satellite_Clicked(object sender, RoutedEventArgs e)
        {
            if (this.satActive.IsVisible == false)
            {
                this.mainContent.Content = new DataInterpreter() { DataContext = this.dataColVM };
                this.homeActive.Visibility = Visibility.Hidden;
                this.satActive.Visibility = Visibility.Visible;
                this.dataActive.Visibility = Visibility.Hidden;
                this.settingsActive.Visibility = Visibility.Hidden;
                this.termActive.Visibility = Visibility.Hidden;
            }
        }

        private void Data_Clicked(object sender, RoutedEventArgs e)
        {
            if (this.dataActive.IsVisible == false)
            {
                this.mainContent.Content = new DataView() { DataContext = this.dataColVM };
                this.homeActive.Visibility = Visibility.Hidden;
                this.satActive.Visibility = Visibility.Hidden;
                this.dataActive.Visibility = Visibility.Visible;
                this.settingsActive.Visibility = Visibility.Hidden;
                this.termActive.Visibility = Visibility.Hidden;
            }
        }

        private void Settings_Clicked(object sender, RoutedEventArgs e)
        {
            if (this.settingsActive.IsVisible == false)
            {
                this.mainContent.Content = new SettingsView() { DataContext = this };
                this.homeActive.Visibility = Visibility.Hidden;
                this.satActive.Visibility = Visibility.Hidden;
                this.dataActive.Visibility = Visibility.Hidden;
                this.settingsActive.Visibility = Visibility.Visible;
                this.termActive.Visibility = Visibility.Hidden;
            }
        }

        private void Terminal_Clicked(object sender, RoutedEventArgs e)
        {
            if (this.termActive.IsVisible == false)
            {
                this.mainContent.Content = new TerminalView() { DataContext = this.termVM };
                this.homeActive.Visibility = Visibility.Hidden;
                this.satActive.Visibility = Visibility.Hidden;
                this.dataActive.Visibility = Visibility.Hidden;
                this.settingsActive.Visibility = Visibility.Hidden;
                this.termActive.Visibility = Visibility.Visible;
            }
        }

        private void MetroWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Width < 720)
            {
                this.homeText.Visibility = Visibility.Collapsed;
                this.dataText.Visibility = Visibility.Collapsed;
                this.satText.Visibility = Visibility.Collapsed;
                this.settingsText.Visibility = Visibility.Collapsed;
                this.termText.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.homeText.Visibility = Visibility.Visible;
                this.dataText.Visibility = Visibility.Visible;
                this.satText.Visibility = Visibility.Visible;
                this.settingsText.Visibility = Visibility.Visible;
                this.termText.Visibility = Visibility.Visible;
            }
        }

        private void MetroWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //// save state
            var appstate = AppController.Instance.appState;

            // tcp settings state
            appstate.IsTcp = this.receiverVM.IsTCP;
            appstate.IpString = this.receiverVM.TcpSettingsViewModel.IpString;
            appstate.IpPort = this.receiverVM.TcpSettingsViewModel.Port;

            // save serial settings without port name, they can be weird
            var serialSettings = this.receiverVM.SerialPortSettings.SerialSettings;
            appstate.SerialBaud = serialSettings.BaudRate;
            appstate.SerialDataBits = serialSettings.DataBits;
            appstate.SerialStopBits = serialSettings.StopBits.ToString("g");
            appstate.SerialParity = serialSettings.Parity.ToString("g");
            appstate.SerialParity = serialSettings.FlowControl.ToString("g");

            appstate.CsvLocation = AppController.Instance.csvLogFolder;

            appstate.Save();

            //// serialize last data model
            var lastdatamodel = AppController.Instance.LastDataModelPath;
            XmlSerializer serializer = new XmlSerializer(typeof(List<DataModel>), new XmlRootAttribute("DataModel"));
            Directory.CreateDirectory(System.IO.Path.GetDirectoryName(lastdatamodel));
            StreamWriter writer = new StreamWriter(lastdatamodel, false, UTF8Encoding.UTF8);
            serializer.Serialize(writer, this.dataColVM.DataCollection);
        }

        private void PickCsvLogFoder()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "CVS Log Folder Location";
            fbd.SelectedPath = this.CsvLocationFolder;
            fbd.ShowDialog();
            this.CsvLocationFolder = fbd.SelectedPath;
            this.dataColVM.ApplyModelChanges();
        }

        public string CsvLocationFolder
        {
            get
            {
                return AppController.Instance.csvLogFolder;
            }
            set
            {
                AppController.Instance.csvLogFolder = value;
                NotifyPropertyChanged("CsvLocationFolder");
            }
        }
    }
}
